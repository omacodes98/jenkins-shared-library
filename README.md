# Jenkins Shared Library 

Create a Jenkins Shared Library 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Extra Screenshots](#extra-screenshots)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Creat a Jenkins Shared Library to extract common build logic

* Create separate Git repository for Jenkins Shared Library project 

* Create functions in the JSL to use in the Jenkins pipline 

* Integrate and use the JSL in Jenkins Pipline 

## Technologies Used 

* Jenkins

* Groovy 

* Docker 

* Git 

* Java 

* Maven 

## Steps 

Step 1: Create a Jenkins shared library 

    mkdir jenkins-shared-library
    cd jenkins-shared-library
    git init 
    git remote add origin git@gitlab.com:omacodes98/jenkins-shared-library.git
    git add .
    git commit -m "Initial commit"
    git push -u origin main 

[Created Library](/images/01_creating_jenkins_shared_library.png)

Step 2: Create var folder in JSL

    cd jenkins-shared-library
    mkdir var 

[Var Folder](/images/02_creating_var_folder_in_repo.png)

Step 3: Create a groovy file in var with logic 

    touch var/buildimage.groovy

[buildimage](/images/03_creating_groovy_file_in_var_with_logic1.png)

Step 4: Push groovy file to git remote repo 

    git status 
    git add .
    git commit -m "added new groovy file with logic"
    git push 

[Push to repo](/images/04_pushing_groovy_files_on_gitlab_remote_repo_for_availability.png)

Step 5: Create Global pipline Library in Jenkins 

[Global pipline library](/images/05_creating_global_pipline_library_in_jenkins.png)

Step 6: Add Shared Library repo to configuration in step 5

[Adding repo in config](/images/06_adding_shared_library_repo.png)

Step 7: Create new branch to test changes 

[New branch](/images/07_creating_a_new_branch_to_test_changes.png)

Step 8: Edit Jenkinsfile by adding the functions in shared library 

    buildjar()
    buildimage()

[Editing Jenkinsfile](/images/08_editing_Jenkins_file_and_adding_the_two_functions_in_shared_library)

Step 9: Scan Multibranch to test shared library 

[Scanned multibranch](/images/09_scanned_multibranch.png)
[Scanned multibranch 2](/images/10_jenkins_shared_lib_branch_status.png)
[Multibranch logs](/images/11_console_logs.png)

## Extra Screenshots

[Adding params](/images/12_adding_parameters_to_buildimage)

[Adding params in jenkinsfile](/images/13_changin_jenkinsfile_to_add_parameters.png)

[Adding Env in buildjar](/images/14_editing_buildjar_to_add_env_var.png)

[Rebuild Image](/images/15_rebuild_image.png)

[Changes to image name](/images/16_changes_to_image_name.png)

[Tag on Dockerhub](/images/17_latest_tags_in_docker_hub.png)

[Created SRC](/images/18_created_src_directory.png)

[Creating groovy script](/images/19_creating_groovy_script_for_logic.png)

[Calling groovy function](/images/20_calling_groovy_function_in_dockergroovy.png)

[After adding classes](/images/21_after_adding_individual_classes.png)

[Rebuild pipeline](/images/21_rebuilding_pipline.png)

## Installation

Run $ apt install nodejs

## Usage 

Run $ java -jar java-maven-app-*.jar

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/jenkins-shared-library.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/jenkins-shared-library

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.